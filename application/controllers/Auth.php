<?php
defined('BASEPATH') OR exit('No direct script access allowed');

class Auth extends CI_Controller {
	
	public function index() {
		check_already_login();
		$this->load->view('login');
	}

	public function process() {

		$post = $this->input->post(null, TRUE);

		if (isset($post['login'])) {
			$this->load->model('user_m');
			$query = $this->user_m->login($post);
			if ($query->num_rows() > 0) {
				$row = $query->row();

				$params = array(
					'userid' => $row->user_id,
					'level' => $row->level
				);
				// echo "<pre>";
				// print_r($params);
				// exit();
				echo "<script>
					alert('Selamat datang, login berhasil');
					window.location='". site_url('dashboard') ."';
				</script>";
			} else {
				echo "<script>
					alert('Login gagal, username / password salah');
					window.location='". site_url('auth') ."';
				</script>";
			}
		}
	}

	public function logout() {

		$params = array('userid', 'level');
		$this->session->unset_userdata($params);
		redirect('auth');
	}
}
